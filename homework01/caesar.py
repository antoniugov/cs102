from string import ascii_lowercase as alphabet


def encrypt_caesar(plaintext: str, shift: int = 3) -> str:
    """
    >>> encrypt_caesar("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar("python")
    'sbwkrq'
    >>> encrypt_caesar("Python3.6")
    'Sbwkrq3.6'
    >>> encrypt_caesar("")
    ''
    """
    ciphertext = ''

    for i, c in enumerate(plaintext.casefold()):

        if not c.isalpha():
            ciphertext += c
            continue

        shifted_index = (alphabet.index(c) + shift) % len(alphabet)
        
        if plaintext[i].isupper():
            ciphertext += alphabet[shifted_index].upper()
        else:
            ciphertext += alphabet[shifted_index]

    return ciphertext


def decrypt_caesar(ciphertext: str, shift: int = 3) -> str:
    """
    Decrypts a ciphertext using a Caesar cipher.

    >>> decrypt_caesar("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar("sbwkrq")
    'python'
    >>> decrypt_caesar("Sbwkrq3.6")
    'Python3.6'
    >>> decrypt_caesar("")
    ''
    """
    plaintext = ''

    for i, c in enumerate(ciphertext.casefold()):

        if not c.isalpha():
            plaintext += c
            continue

        shifted_index = (alphabet.index(c) - shift) % len(alphabet)

        if ciphertext[i].isupper():
            plaintext += alphabet[shifted_index].upper()
        else:
            plaintext += alphabet[shifted_index]

    return plaintext
