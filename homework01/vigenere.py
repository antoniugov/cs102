from string import ascii_lowercase as alphabet


def encrypt_vigenere(plaintext: str, keyword: str) -> str:
    """
    Vigenere encryption implementation.

      plaintext - original string to be encrypted
      keyword - string which will be used to calculate corresponding shifts

    >>> encrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> encrypt_vigenere("python", "a")
    'python'
    >>> encrypt_vigenere("ATTACKATDAWN", "LEMON")
    'LXFOPVEFRNHR'
    """
    # Extend keyword to match plaintext.
    keyword_extended = keyword * (len(plaintext) // len(keyword) + 1)
    # tabula_recta holds shift values for each character.
    tabula_recta = {c: shift for (shift, c) in enumerate(alphabet)}
    ciphertext = ''

    for (origin, target) in zip(plaintext, keyword_extended.casefold()):

        if not origin.isalpha():
            ciphertext += origin
            continue

        origin_pos = alphabet.index(origin.casefold())
        target_pos = tabula_recta[target]
        shift = (origin_pos + target_pos) % len(alphabet)
        enciphered = alphabet[shift]
        ciphertext += enciphered.upper() if origin.isupper() else enciphered

    return ciphertext


def decrypt_vigenere(ciphertext: str, keyword: str) -> str:
    """
    Vigenere decryption implementation.

      ciphertext - encrypted string to be decrypted
      keyword - string which will be used to calculate corresponding shifts

    >>> decrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> decrypt_vigenere("python", "a")
    'python'
    >>> decrypt_vigenere("LXFOPVEFRNHR", "LEMON")
    'ATTACKATDAWN'
    """
    # Extend keyword to match ciphertext.
    keyword_extended = keyword * (len(ciphertext) // len(keyword) + 1)
    # tabula_recta holds shift values for each character.
    tabula_recta = {c: shift for (shift, c) in enumerate(alphabet)}
    plaintext = ''

    for (origin, target) in zip(ciphertext, keyword_extended.casefold()):

        if not origin.isalpha():
            plaintext += origin
            continue

        origin_pos = alphabet.index(origin.casefold())
        target_pos = tabula_recta[target]
        shift = (origin_pos - target_pos) % len(alphabet)
        deciphered = alphabet[shift]
        plaintext += deciphered.upper() if origin.isupper() else deciphered

    return plaintext
